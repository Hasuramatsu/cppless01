#include <iostream>

class Animal
{
public:
	Animal() {}
	virtual void Voice() {}
};

class Dog : public Animal
{
public:
	Dog() {}
	void Voice() override
	{
		std::cout << "Woof!\n";
	}
};

class Cat : public Animal
{
public:
	Cat() {}
	void Voice() override
	{
		std::cout << "Nyaa~\n";
	}
};

class Cow : public Animal
{
public:
	Cow() {}
	void Voice() override
	{
		std::cout << "Moooo...\n";
	}
};




int main()
{
	const int Size = 3;
	Animal* animal[Size]{ new Dog, new Cat, new Cow };
	for (int i = 0; i < Size; i++)
	{
		animal[i]->Voice();
		delete animal[i];
	}

	return 0;
}